
# What You'll Be Building
# In this project, we'll build a program that takes a user's input, then builds
# a hash from that input. Each key in the hash will be a word from the user;
# each value will be the number of times that word occurs. For example, if our
# program gets the string "the rain in Spain falls mainly on the plain," it will
# return
#
# the 2
# falls 1
# on 1
# mainly 1
# in 1
# rain 1
# plain 1
# Spain 1
#
# A visual representation of data like this is called a histogram.

# Tres tristes tigres comen trigo de un trigal en tres tristes trastos.
# En tres tristes trastos de un trigal comen trigo tres tristes tigres.
puts "Como iba ese trabalenguas de los tigres?"
text        = gets.chomp
words       = text.split
frequencies = Hash.new(0) # Seed the hash with 0 for new values.

# Count the frequency of each word.
words.each { | word | frequencies[word] += 1 }
# Sort the hash by the frequency value.
frequencies = frequencies.sort_by {|_, b| b}.reverse
# Print each word-frequency pair.
frequencies.each { |k, v| puts "#{k}: #{v}" }
