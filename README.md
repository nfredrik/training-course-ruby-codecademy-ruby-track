# Codecademy's Ruby Track

This repository contains code generated from the projects of the Ruby track in
[Codecademy](http://www.codecademy.com/en/tracks/ruby). These are little practices but, most of the time, these are improved versions as result of the development of functionality suggested on the lessons.

## List of Projects
- **Putting the Form in Formatting**: A small program that reads a user's input and correct his or her capitalization.
- **Thith Meanth War!**: Modify a user's input and return it to them. In this project, we'll make them sound like Daffy Duck!
- **Redacted!**: Program that searches a string of text for unwanted words, if it finds any, replaces it with the word "redacted." Just like that, you're a spy!
- **Create a Histogram**: Program that reads a block of text and tells us how many times each word appears.
- **Ordering Your Library**: Design a single Ruby method to sort large quantities of data in either ascending or descending order.
- **A Night at the Movies**: Construct a program that displays, adds, updates, and removes movie ratings!
- **The Refactor Factory**: Use step-by-step refactoring to vastly improve the readability and structure of a program.
- **Virtual Computer**: In this project, we'll use Ruby classes to create our own imaginary computer that stores data!
- **Banking on Ruby**: Program that can store, update, and display a bank account balance.

---
**Note**: The description of the problems is material from Codecademy and is included in this repository under _Fair Use_. Unless otherwise stated, all the code included in this repository is released to the public domain under the [Creative Commons CC0 1.0 Universal License][10].

  [10]: https://creativecommons.org/publicdomain/zero/1.0/