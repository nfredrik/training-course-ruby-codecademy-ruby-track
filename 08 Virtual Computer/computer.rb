
# What You'll Be Building
# Now that you've learned all about classes and objects in Ruby, you can create
# any kind of Ruby object your heart desires. In this project, we'll use our
# newfound knowledge to create a class, Computer, and generate instances of that
# class that can manipulate imaginary files for us.

class Computer
  @@users = {}

  def initialize(username, password, admin=false)
    @username = username
    @password = password
    @admin    = admin
    @files    = {}

    @@users[username] = { password: password, admin: admin }
  end

  def create(filename, content)
    time = Time.now
    @files[filename] = { content: content, time: time }

    puts "User #{@user} created the file '#{filename}' on #{time}."
  end

  def delete(filename)
    if @files.include?(filename)
      @files.delete(filename)

      puts "The file '#{filename}' was deleted."
    else
      puts "The file '#{filename}' was not found."
    end
  end

  def modify(filename, new_content)
    if @files.include?(filename)
      time = Time.now
      @files[filename][:time]    = time
      @files[filename][:content] = new_content

      puts "Changes to file '#{filename}' were saved."
    else
      puts "The file '#{filename}' was not found."
    end
  end

  def Computer.get_users
    @@users
  end

  def get_files
    @files
  end
end

computer1 = Computer.new("Rojo", "can't hug every cat", true)
computer2 = Computer.new("Juanjo", "theslayer")

computer1.create("index.html", "<html>")
computer2.create("picture.jpg", "bits, bits")
puts computer1.get_files
puts computer2.get_files

computer1.modify("index.html", "<!doctype html>")
computer2.modify("picture.jpg","bits, bits, bits")
puts computer1.get_files
puts computer2.get_files

computer1.delete("index.html")
computer2.delete("picture.jpg")

puts Computer.get_users
